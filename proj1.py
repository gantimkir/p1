# -*- coding: utf-8 -*-
import socket
import json
from tkinter import *
import proj2_lib1 as gui



window = Tk()
window.title("Знакомство с Quik")
window.geometry("650x300+10+10")

gui.create_controls(window)
window.mainloop()

# CRLF = "\r\n\r\n"
# host = '127.0.0.1'
# port_requests = 34130
# port_callbacks = 34131
# sok_requests = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sok_callbacks = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sok_callbacks.connect((host , port_callbacks))
# sok_requests.connect((host , port_requests))
# request = {"data":"SPBFUT|SiH2","id":"1","cmd":"Subscribe_Level_II_Quotes","t":""}
# raw_data = json.dumps(request)
# sok_requests.sendall((raw_data+CRLF).encode())
# print("Subscribe is sent")
#
# data = b""
# bufsize = 1024
# while(True):
#     print("SPBFUT|SiH2")
#     packet = sok_requests.recv(bufsize)
#     data += packet
#     if len(packet) < bufsize:
#         break
# data = json.loads(data.decode('cp1251'))
# print(data)
#
# request = {"data":"SPBFUT|SiH2","id":"1","cmd":"GetQuoteLevel2","t":""}
# raw_data = json.dumps(request)
# sok_requests.sendall((raw_data+CRLF).encode())
# print("getQuote request is sent")
#
# data = b""
# bufsize = 1024
# while(True):
#     print("SPBFUT|SiH2")
#     packet = sok_requests.recv(bufsize)
#     data += packet
#     if len(packet) < bufsize:
#         break
# data = json.loads(data.decode('cp1251'))
# print(data)