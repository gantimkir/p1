import  json

import pygal
from pygal.style import RotateStyle, LightColorizedStyle
from pygal_maps_world.i18n import COUNTRIES

def getCountry(c_name):
    for code, name in COUNTRIES.items():
        if name == c_name:
            return code
    return None

file = "population_data.json"
with open(file) as f:
    pop = json.load(f)
    ccpop = {}
    for p in pop:
        if p['Year'] == '2010':
            c_n = p['Country Name']
            pnum = int(float(p['Value']))
            cc = getCountry(c_n)
            if cc:
                ccpop[cc] = pnum
            else:
                print('Error: ' + c_n)
            ccpop1, ccpop2, ccpop3 = {}, {}, {}
            for c, p in ccpop.items():
                if p < 10000000:
                    ccpop1[c]=p
                elif p < 100000000:
                    ccpop2[c]=p
                else:
                    ccpop3[c]=p
            wms = RotateStyle('#336699')
            wm = pygal.maps.world.World(style=wms, base_style=LightColorizedStyle)
            wm.title='111'

            wm.add('0-10m', ccpop1)
            wm.add('10m-1bn', ccpop2)
            wm.add('>1bn', ccpop3)

            wm.render_to_file('world.svg')