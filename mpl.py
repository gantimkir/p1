import matplotlib.pyplot as plt

# input_values = [1, 2, 3, 4, 5, 6, 7]
# sq = [1, 4, 9, 16, 25, 36, 49]
# plt.plot(input_values, sq, linewidth = 5)
# plt.title("Square Numbers", fontsize=24)
# plt.xlabel("Value", fontsize=14)
# plt.ylabel("Square of Value", fontsize=14)
# plt.tick_params(axis='both', labelsize=14)
# plt.show()

# x_values = list(range(1, 201))
# y_values = [x**2 for x in x_values]
# plt.scatter(x_values, y_values, c=y_values, cmap=plt.cm.Blues,
#     edgecolor='none', s=1)
# # Назначение заголовка диаграммы и меток осей.
# plt.title("Square Numbers", fontsize=24)
# plt.xlabel("Value", fontsize=14)
# plt.ylabel("Square of Value", fontsize=14)
# plt.axis([0, 201, 0, 40000])
# # Назначение размера шрифта делений на осях.
# plt.tick_params(axis='both', which='major', labelsize=14)
# plt.show()

from random import choice
class RandomWalk():

    def __init__(self, num_points = 1000):
        self.num_points = num_points

        self.x_values = [0]
        self.y_values = [0]

    def fill_walk(self):
        while len(self.x_values) < self.num_points:
            x_dir = choice([1, -1])
            x_dist = choice([0, 1, 2, 3, 4])
            x_step = x_dir * x_dist
