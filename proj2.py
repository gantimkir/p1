# -*- coding: utf-8 -*-
import socket
import json
from tkinter import *
import proj2_lib1

window = Tk()
window.title("Знакомство с Quik")
window.geometry("650x300+10+10")

# gui variables
padx = 5
pady = 5

# quik data
classes = []

# socket variables
host = '127.0.0.1'
port_requests = 34130
port_callbacks = 34131
sok_requests = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sok_callbacks = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sok_callbacks.connect((host, port_callbacks))
sok_requests.connect((host, port_requests))

def get_classes_btn_click():
    classes = proj2_lib1.get_classes_list(sok_requests)
    for cls in classes:
        lst_classes.insert(END, cls)

def lst_classes_click(event):
    sel = lst_classes.curselection()
    if len(sel) > 0:
        cls=lst_classes.get(sel[0])
        print(cls)
        securities = proj2_lib1.get_securities_list(sok_requests, cls)
        # print(securities)
        lst_sec.delete(0,'end')
        for sec in securities:
            lst_sec.insert(END, sec)


def lst_sec_click(event):
    sel = lst_sec.curselection()
    if len(sel) > 0:
        sec=lst_sec.get(sel[0])
        print(sec)

btn_get_classes = Button(window, text="Get classes", fg="green", command=get_classes_btn_click)
btn_get_classes.grid(column=0, row=0, padx=padx, pady=pady)

lst_classes = Listbox(window)
lst_classes.grid(column=0, row=1, padx=padx, pady=pady)
lst_classes.bind('<<ListboxSelect>>', lst_classes_click)


lst_sec = Listbox(window)
lst_sec.grid(column=1, row=1, padx=padx, pady=pady)
lst_sec.bind('<<ListboxSelect>>', lst_sec_click)


window.mainloop()
