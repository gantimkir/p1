import sys
import pygame
from settings import Settings
import game_functions as gf
from ship import  Ship
from pygame.sprite import Group
from alien import Alien




def run_game():
    # Инициализирует игру и создает объект экрана.
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")
    ship = Ship(ai_settings, screen)
    alien = Alien(ai_settings, screen)

    bullets = Group()
    aliens = Group()
    gf.create_fleet(ai_settings, screen, aliens)

    # Запуск основного цикла игры.
    while True:

        # Отслеживание событий клавиатуры и мыши.
        gf.check_events(ai_settings, screen, ship, bullets)
        ship.update()
        gf.update_bullets(bullets)
        gf.update_screen(ai_settings, screen,ship, aliens, bullets)



run_game()


