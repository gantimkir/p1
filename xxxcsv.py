import csv
from matplotlib import pyplot as plt
from datetime import datetime

filename = 'sitka_weather_07-2014.csv'

with open(filename) as f:
    reader = csv.reader(f)
    h_row = next(reader)
    for i, c_h in enumerate(h_row):
        print(i, c_h)
    dates = []
    highs = []
    for row in reader:
        c_date = datetime.strptime(row[0],"%Y-%m-%d")
        dates.append(c_date)
        highs.append(row[1])
    print(highs)

fig = plt.figure(dpi=128, figsize=(6,3))
plt.plot(dates,highs, c='red')

plt.title('High temperatures', fontsize=8)
plt.xlabel('', fontsize=8)
plt.ylabel('Temp. (F)', fontsize=8)
plt.tick_params(axis='both',which='major',labelsize=16)
plt.show()
