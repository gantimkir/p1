import csv
from matplotlib import pyplot as plt
from datetime import datetime

filename = 'sitka_weather-2014.csv'

with open(filename) as f:
    reader = csv.reader(f)
    h_row = next(reader)
    for i, c_h in enumerate(h_row):
        print(i, c_h)
    dates = []
    highs, lows = [], []
    for row in reader:
        try:
            c_date = datetime.strptime(row[0],"%Y-%m-%d")
            low = int(row[3])
            high = int(row[1])
        except ValueError:
            print(c_date,'missing data')
        else:
            dates.append(c_date)
            lows.append(low)
            highs.append(row[1])
    print(highs)

fig = plt.figure(dpi=150, figsize=(6,3))
plt.plot(dates,highs, c='red', alpha=0.5)
plt.plot(dates,lows, c='blue', alpha=0.5)
plt.fill_between(dates, highs, lows, facecolor='blue',alpha=0.1)


plt.title('High/low temperatures', fontsize=8)
plt.xlabel('', fontsize=8)
plt.ylabel('Temp. (F)', fontsize=8)
plt.tick_params(axis='both',which='major',labelsize=16)
plt.show()
