import json
import socket


def get_classes_list(sok_requests):
    CRLF = "\r\n\r\n"
    request = {"data":"","id":"1","cmd":"getClassesList","t":""}
    raw_data = json.dumps(request)
    sok_requests.sendall((raw_data+CRLF).encode())
    data = b""
    bufsize = 1024
    while(True):
        packet = sok_requests.recv(bufsize)
        data += packet
        if len(packet) < bufsize:
            break
    data = json.loads(data.decode('cp1251'))
    return data['data'].split(",")

def get_securities_list(sok_requests, cls):
    CRLF = "\r\n\r\n"
    request = {"data": cls,"id":"1","cmd":"getClassSecurities","t":""}
    raw_data = json.dumps(request)
    sok_requests.sendall((raw_data+CRLF).encode())
    data = b""
    bufsize = 1024
    while(True):
        packet = sok_requests.recv(bufsize)
        data += packet
        if len(packet) < bufsize:
            break
    data = json.loads(data.decode('cp1251'))
    return data['data'].split(",")
