from random import randint
import pygal

class Die():

        def __init__(self, num_sides = 6):
            self.num_sides = num_sides

        def roll(self):
            return randint(1, self.num_sides)

die1 = Die()
die2 = Die(10)


results = []
for roll_num in range(50000):
    result = die1.roll() + die2.roll()
    results.append(result)

print(results)

frequencies = []
max_r = die1.num_sides + die2.num_sides

for value in range(2, max_r + 1):
    frequency = results.count(value)
    frequencies.append(frequency)
print(frequencies)

hist = pygal.Bar()

hist.title = 'Results 2x'
hist.x_labels = ['2','3','4','5','6','7','8','9','10','11','12']
hist.x_title = 'results'
hist.y_title = 'freq'

hist.add('D6', frequencies)
hist.render_to_file('die2.svg')